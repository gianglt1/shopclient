import React from 'react';
import Header from 'component/Header/Header';
import HomePage from 'pages/home-page';
import LoginPage from 'pages/login-page';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Footer from './home-page/component/footer';


const MainPage = (props) => {

  return (
    <div className="view">
      <div className="header">
        <Header />
      </div>
      <div className="content">
        <BrowserRouter>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
          </Switch>
        </BrowserRouter>
      </div>
      <div className="footer">
        <Footer />
      </div>
    </div>
  )
}

export default MainPage;