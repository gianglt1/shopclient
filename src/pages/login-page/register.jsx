import { Modal, Form } from 'react-bootstrap';
import { GeneralButton } from 'component/Common/Components/button/button';
import './style.css';



const RegisterModal = (props) => {

    const handleClose = () => props.setShow(false);

    return (
        <Modal show={props.show} onHide={handleClose} size="lg">
            <Modal.Header closeButton bsPrefix='main-login-content-modal-header' />
            <Modal.Body>
                <Form>
                    <h3>Đăng ký thành viên</h3>
                    {['radio'].map((type) => (
                        <div key={`inline-${type}`} className="main-login-content-form-radio">
                            <Form.Check
                                inline
                                label="Số điện thoại"
                                name="group1"
                                type={type}
                                id={`inline-${type}-1`}
                                defaultChecked
                            />
                            <Form.Check
                                inline
                                label="Mã số thẻ thành viên"
                                name="group1"
                                type={type}
                                id={`inline-${type}-2`}
                            />
                        </div>
                    ))}
                    <div className="main-login-content-form-input">
                        <span>Vui lòng nhập số điện thoại để nhận mã xác nhận:</span>
                        <div className="main-login-content-form-input-text-box">
                            <Form.Control type="text" placeholder="Ví dụ: 0987654321" />
                            <div className="main-login-content-form-input-button">
                                <GeneralButton des="Gửi" />
                            </div>
                        </div>
                    </div>
                    <div className="main-login-content-form-input">
                        <span> Vui lòng nhập mã xác nhận gồm 6 chữ số:</span>
                        <div className="main-login-content-form-input-text-box">
                            <Form.Control type="text" placeholder="Ví dụ: 123456" />
                            <div className="main-login-content-form-input-button">
                                <GeneralButton des="Xác nhận" />
                            </div>
                        </div>
                    </div>
                </Form>
            </Modal.Body>
        </Modal>
    )
}

export default RegisterModal;