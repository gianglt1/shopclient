import Header from "component/Header/Header";
import Footer from '../home-page/component/footer';
const { default: LoginContent } = require("./login")
import RegisterModal from "./register";
import {useState} from 'react';
import LoadingOverlay from 'component/LoadingPage/LoadingOverlay';
// import { Button} from 'react-bootstrap';




const LoginPage = () => {
    const [isShowRegisterModal, setIsShowRegisterModal] = useState(false);
    const [isShowLoading, setIsShowLoading] = useState(false);

    return (
        <div className="view">
            <div className="header">
                <Header />
            </div>
            <div className="content">
                <LoginContent setShowRegisterModal={setIsShowRegisterModal} setIsShowLoading={setIsShowLoading}/>
                <RegisterModal show={isShowRegisterModal} setShow={setIsShowRegisterModal}/>
            </div>
            <LoadingOverlay show={isShowLoading} handleClose={setIsShowLoading}/>
            <Footer />
        </div>
    )
}

export default LoginPage;