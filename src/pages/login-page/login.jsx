import { GeneralButton } from 'component/Common/Components/button/button';
import './style.css';


const LoginContent = (props) => {

    const onSubmit = (e) => {
        e.preventDefault()
        const user = document.getElementById("user")
        const password = document.getElementById("password")
        console.log("user - password", user, password)
    }

    return (
        <div className="main-login">
            <div className="main-login-content">
                <div className="main-login-content-box">
                    <div className="sign-in-content">
                        <h4>Đăng nhập thành viên</h4>
                        <form onSubmit={onSubmit}>
                            <input type="email" id="user" placeholder="ex: 123456789/sample@innisfree.com" />
                            <input type="password" id="password" placeholder="Vui lòng nhập mật khẩu" />
                            <a href="#" id="forget"><span>Bạn quên mật khẩu</span></a>
                            {/* <div className="sign-in-content-btn"> */}
                            {/* <div className="sign-in-content-btn-box"> */}
                            <GeneralButton des="Đăng nhập" />
                            {/* </div> */}
                            {/* </div> */}
                        </form>
                        <p>Đăng nhập nhanh/ Đăng ký bằng tài khoản mạng xã hội:</p>
                        <p className="note">Có thể đăng nhập nhanh hoặc đăng ký thành viên mới bằng tài khoản mạng xã hội.</p>
                        <p className="note">Thành viên innisfree chưa liên kết với tài khoản mạng xã hội có thể đăng nhập và thiết lập liên kết một cách nhanh chóng.</p>
                    </div>
                    <div className="sign-up-content">
                        <h4>Đăng ký thành viên mới</h4>
                        <p className="note">
                            Đăng ký ngay để mua sắm dễ dàng hơn và tận hưởng thêm nhiều ưu đãi độc quyền cho thành viên innisfree.
                        </p>
                        <div className="sign-up-content-btn">
                            <GeneralButton des="Đăng ký thành viên" onClick={() => { props.setShowRegisterModal(true) }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginContent;