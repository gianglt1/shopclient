import React from 'react';
import { person } from 'assets/icon/icon.jsx';
import { GeneralButton, GeneralButtonConverse } from 'component/Common/Components/button/button';
import '../style.css';


const LoginBar = () => {
    return (
        <div className="main-content-header">
            <div className="main-content-header-description">
                <span className="img-header"><img className="img3" src={person} alt="person" /></span>
                <span>Xin chào, mời bạn đăng nhập để tận hưởng ưu đãi nhé!</span>
            </div>
            <div className="main-content-header-button">
                {/* <div className="login-bg"><a href="#" className="login">Đăng nhập <img src={arrowRight} alt="arrow-right" /></a></div>
                <div className="signup-bg"><a href="#" className="signup">Đăng ký thành viên <img src={arrowRight} alt="arrow-right" /> </a></div> */}
                <div className="login-bg">
                    <GeneralButtonConverse des="Đăng nhập" />
                </div>
                <div className="signup-bg">
                    <GeneralButton des="Đăng ký thành viên" />
                </div>
            </div>
        </div>
    )
}

export default LoginBar;