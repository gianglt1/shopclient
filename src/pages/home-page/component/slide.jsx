import React from 'react';
import { Carousel } from 'react-bootstrap';
import slider1 from 'assets/img/slider/slider-1.jpg';
import slider2 from 'assets/img/slider/slider-2.jpg';
import slider3 from 'assets/img/slider/slider-3.jpg';
import slider4 from 'assets/img/slider/slider-4.jpg';
import slider5 from 'assets/img/slider/slider-5.jpg';
import slider6 from 'assets/img/slider/slider-6.jpg';


const SlideShow = () => {

    return (
        <div className="main-slider">
            <div className="main-slider-cover">
                <Carousel interval={1000000} className="main-slider-content">
                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider1} alt="sld1" />
                            </div>
                            <div className="main-slider-description">
                                <h3>black tea youth enhancing line</h3>
                                <p>Siêu phẩm chống lão hóa</p>
                            </div>
                        </a>
                    </Carousel.Item>

                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider2} alt="sld2" />
                            </div>
                            <div className="main-slider-description">
                                <h3>phục hồi cấp tốc đón năm mới</h3>
                                <p>Khám phá ưu đãi chỉ có trong tháng 1</p>
                            </div>
                        </a>
                    </Carousel.Item>
                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider3} alt="sld3" />
                            </div>
                            <div className="main-slider-description">
                                <h3>black tea youth enhancing ampoule</h3>
                                <p>Phục hồi vẻ đẹp nguyên bản của làn da</p>
                            </div>
                        </a>
                    </Carousel.Item>
                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider4} alt="sld4" />
                            </div>
                            <div className="main-slider-description">
                                <h3>black tea youth enhancing ampoule</h3>
                                <p>Phục hồi vẻ đẹp nguyên bản của làn da</p>
                            </div>
                        </a>
                    </Carousel.Item>
                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider5} alt="sld5" />
                            </div>
                            <div className="main-slider-description">
                                <h3>black tea youth enhancing ampoule</h3>
                                <p>Phục hồi vẻ đẹp nguyên bản của làn da</p>
                            </div>
                        </a>
                    </Carousel.Item>
                    <Carousel.Item className="main-slider-item">
                        <a href="#">
                            <div className="main-slider-image">
                                <img src={slider6} alt="sld6" />
                            </div>
                            <div className="main-slider-description">
                                <h3>black tea youth enhancing ampoule</h3>
                                <p>Phục hồi vẻ đẹp nguyên bản của làn da</p>
                            </div>
                        </a>
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
    )
}

export default SlideShow;