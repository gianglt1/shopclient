import React from 'react';
import Slider from "react-slick";
import prod1 from 'assets/img/product/prod_1_1.png';
import prod2 from 'assets/img/product/prod_1_2.png';
import prod3 from 'assets/img/product/prod_1_3.png';
import greentea01 from "assets/img/product/greentea01.png";
import iconReview from 'assets/img/review/ico_superstar.png';
import imgReview from 'assets/img/review/5004_10699_attimg_origin.jpeg';
import brand1 from 'assets/img/brand/brand1.jpg';
import guide1 from 'assets/img/service/ico_guide1.png';
import BoundingBox, { StoryContent, ReviewContent, AboutMeContent, ServiceContent } from 'component/Common/Components/box/box';
import { CatalogContent, CatalogDescriptionContent } from 'component/Common/Components/Catalog/catalog';
import { caretRightFill, caretLeftFill } from 'assets/icon/icon';
import { Row, Col } from 'react-bootstrap';

const detail = [
    {
        img: prod1,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Bộ lịch sáng tạo innisfree Calendar 2021',
        amount: { sale: '150', price: '150' }
    },
    {
        img: prod2,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Tinh chất phục hồi da và chống lão trà đen innisfree Black Tea Youth Enhancing Ampoule 30ml',
        amount: { sale: '', price: '150' }
    },
    {
        img: prod3,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Bộ tẩy tế bào chết innisfree Green Barley Gommage Routine Set 159mL',
        amount: { sale: '', price: '150' }
    },
    {
        img: prod1,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Bộ lịch sáng tạo innisfree Calendar 2021',
        amount: { sale: '150', price: '150' }
    },
    {
        img: prod2,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Tinh chất phục hồi da và chống lão trà đen innisfree Black Tea Youth Enhancing Ampoule 30ml',
        amount: { sale: '', price: '150' }
    },
    {
        img: prod3,
        tag: { new: 'mới', promo: 'promo', best: 'best' },
        content: 'Bộ tẩy tế bào chết innisfree Green Barley Gommage Routine Set 159mL',
        amount: { sale: '', price: '150' }
    },
]

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: <img src={caretLeftFill} className="caret-left" />,
    nextArrow: <img src={caretRightFill} className="caret-right" />,
};

const MainCatalogContent = (props) => {




    return (
        <CatalogContent title={props.title}>
            <CatalogDescriptionContent>
                <Slider {...settings}>
                    {Object.keys(props.detail || []).map((index) => {
                        return <BoundingBox tag={props.detail[index].tag}
                            img={props.detail[index].img}
                            alt={index}
                            description={props.detail[index].content}
                            amount={props.detail[index].amount}
                            key={index} 
                            handleModal={props.setVisible}/>
                    })}
                </Slider>
            </CatalogDescriptionContent>
        </CatalogContent>
    )
}

const storyDetail = {
    title: "Trà xanh",
    info: "Nước trà tươi 100% không thuốc trừ sâu và búp trà quý giá chỉ có thể thu hái từ những cây trà hơn 7 năm tuổi, trên cánh đồng trà xanh không thuốc trừ sâu tại Jeju, cung cấp độ ẩm và dưỡng chất cho da đồng thời duy trì độ ẩm trong thời gian dài giúp làn da săn chắc khỏe mạnh.",
    data: [
        {
            image: greentea01,
            detail: "Tinh chất dưỡng ẩm trà xanh",
        },
        {
            image: greentea01,
            detail: "Tinh chất dưỡng ẩm trà xanh",
        },
        {
            image: greentea01,
            detail: "Tinh chất dưỡng ẩm trà xanh",
        }
    ]
}

const MainStoryContent = (props) => {
    return (
        <CatalogContent title={props.title}>
            <StoryContent title={storyDetail.title} info={storyDetail.info} detail={storyDetail.data} />
        </CatalogContent>
    )
}

const reviewDetail = [
    {
        icon: iconReview,
        name: 'Garam',
        major: 'Super Star',
        date: '2020-12-25',
        img: imgReview,
        star: 4,
        title: 'Đánh giá CAO CẤP ẤM',
        description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
        tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
        like: 40,
        comment: 21,
    },
    {
        icon: iconReview,
        name: 'Garam',
        major: 'Super Star',
        date: '2020-12-25',
        img: imgReview,
        star: 4,
        title: 'Đánh giá CAO CẤP ẤM',
        description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
        tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
        like: 40,
        comment: 21,
    },
    {
        icon: iconReview,
        name: 'Garam',
        major: 'Super Star',
        date: '2020-12-25',
        img: imgReview,
        star: 4,
        title: 'Đánh giá CAO CẤP ẤM',
        description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
        tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
        like: 40,
        comment: 21,
    },
    {
        icon: iconReview,
        name: 'Garam',
        major: 'Super Star',
        date: '2020-12-25',
        img: imgReview,
        star: 4,
        title: 'Đánh giá CAO CẤP ẤM',
        description: `Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
        Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
        Các bạn da khô nên tham khảo qua dòng này nhé ❤❤❤`,
        tag: ["Da Khô", "Làm Sạch ", "Dưỡng Ẩm"],
        like: 40,
        comment: 21,
    }
]

const MainReviewContent = (props) => {
    return (
        <CatalogContent title={props.title}>
            <CatalogDescriptionContent>
                <Slider {...settings}>
                    {Object.keys(reviewDetail || []).map((index) => {
                        return <ReviewContent key={index} detail={reviewDetail[index]} />
                    })}
                </Slider>
            </CatalogDescriptionContent>
        </CatalogContent>
    )
}

const aboutDetail = [
    {
        img: brand1,
        title: "Giới thiệu về innisfree",
        description: "innisfree được thành lập vào tháng 1 năm 2000, là thương hiệu mỹ phẩm thừa hưởng những tinh túy của thiên nhiên từ đảo Jeju, hòn đảo tràn ngập trong không khí trong lành và tươi mát. Jeju được UNESCO công nhận là di sản thiên nhiên thế giới đầu tiên của Hàn Quốc.",
    },
    {
        img: brand1,
        title: "Giới thiệu về innisfree",
        description: "innisfree được thành lập vào tháng 1 năm 2000, là thương hiệu mỹ phẩm thừa hưởng những tinh túy của thiên nhiên từ đảo Jeju, hòn đảo tràn ngập trong không khí trong lành và tươi mát. Jeju được UNESCO công nhận là di sản thiên nhiên thế giới đầu tiên của Hàn Quốc.",
    },
]

const MainAboutMeContent = (props) => {

    return (
        <CatalogContent title={props.title}>
            <Row>
                {Object.keys(aboutDetail).map((index) => {
                    return <Col key={index}>
                        <AboutMeContent img={aboutDetail[index].img} title={aboutDetail[index].title} description={aboutDetail[index].description} />
                    </Col>
                })}
            </Row>

        </CatalogContent>
    )
}

const serviceDetail = [
    [
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
    ],
    [
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
        {
            img: guide1,
            title: "Đăng ký thành viên",
            description: "Đăng ký thành viên innisfree ngay chỉ với vài bước đơn giản để tận hưởng vô vàn ưu đãi.",
        },
    ],
]

const MainServiceContent = (props) => {
    return <CatalogContent title={props.title}>
        {Object.keys(serviceDetail).map((index) => {
            return <Row key={index}>
                {Object.keys(serviceDetail[index]).map((indexItem) => {
                    return <Col key={indexItem}>
                        <ServiceContent img={serviceDetail[index][indexItem].img} title={serviceDetail[index][indexItem].title} description={serviceDetail[index][indexItem].description} />
                    </Col>
                })}
            </Row>
        })}
    </CatalogContent>
}

const MainContent = (props) => {

    return (
        <div className="main-content-catalog">
            <MainCatalogContent detail={detail} title="Sản phẩm Ưu đãi" setVisible={props.setVisible}/>
            <MainStoryContent title="Câu chuyện từ đảo jeju" />
            <MainReviewContent title="Review HOT" />
            <MainAboutMeContent title="Về chúng tôi" />
            <MainServiceContent title="Mua hàng và Ưu đãi thành viên" />
        </div>
    )
}

export default MainContent;