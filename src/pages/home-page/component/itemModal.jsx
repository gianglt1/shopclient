
import item1 from 'assets/img/item/main/28_ID0101_1.jpg';
import item2 from 'assets/img/item/main/28_ID0101_2.jpg';
import item3 from 'assets/img/item/main/28_ID0101_3.jpg';
import item4 from 'assets/img/item/main/28_ID0101_4.jpg';
import { useState, useMemo, useCallback } from 'react';
import Slider from "react-slick";
import { caretRightFill, caretLeftFill, heart, PE1101_attimg_origin, PE1102_attimg_origin, PE1104_attimg_origin, PE1201_attimg_origin } from 'assets/icon/icon';
import { CatalogDescriptionContent } from 'component/Common/Components/Catalog/catalog';
import { ProductionModal } from 'component/Common/Components/production/production';
import { FitInfo } from 'component/Common/Components/info/info';
const { default: HeaderBoundingBox } = require("../../../component/Common/Components/headerBox/headerBox")
import { FormatAmount } from 'component/Common/util/format';
import { GeneralDisplayButtonBox, GeneralDisplayButtonBoxConverse } from 'component/Common/Components/button/button';
import { Modal } from 'react-bootstrap';

const itemSlider = (props) => {
  const [activeSlide, setActivedSlide] = useState(0);

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    beforeChange: (_, next) => setActivedSlide(next),
    prevArrow: <img src={caretLeftFill} className="caret-left" />,
    nextArrow: <img src={caretRightFill} className="caret-right" />,
    focusOnSelect: true,
  };
  return (
    <div className="item-slider-container">
      <div className="item-slider-main-screen">
        <img src={require(`../../../assets/img/item/main/28_ID0101_${activeSlide + 1}.jpg`).default} alt="main-screen" />
      </div>
      <CatalogDescriptionContent>
        <Slider {...settings}>
          <ProductionModal img={item1} alt="item" />
          <ProductionModal img={item2} alt="item" />
          <ProductionModal img={item3} alt="item" />
          <ProductionModal img={item4} alt="item" />
        </Slider>
      </CatalogDescriptionContent>
    </div>
  )
}

const ItemModal = (props) => {
  const tag = { new: 'mới', promo: 'promo', best: 'best' };
  const renderStar = useMemo(() => {
    const star = [];
    for (let i = 0; i < 5; i++) {
      if (i < props.detail.star) {
        star[i] = <span className="review-star">*</span>
        continue
      }
      star[i] = <span className="review-star">||</span>
    }
    return star
  }, [])

  const renderFit = useCallback(
    (fits) => {
      return <div className="list-fit">
        {Object.keys(fits || []).map(key => {
          switch (fits[key]) {
            case 'DA':
              return <FitInfo image={PE1101_attimg_origin} info="Dưỡng ẩm" />
            case 'CLH':
              return <FitInfo image={PE1102_attimg_origin} info="Chống lão hóa" />
            case 'DS':
              return <FitInfo image={PE1104_attimg_origin} info="Dưỡng sáng" />
            case 'MLD':
              return <FitInfo image={PE1201_attimg_origin} info="Mọi loại da" />
            default:
              return null;
          }
        })}
      </div>
    },
    [],
  )

  // const valueChange = (value) => {
  //   const regex = new RegExp(/^(\d)$/);
  //   const counter = String(value).replace(regex, '')
  // }

  // const renderCounter = useCallback(()=>{
  //   return <div>
  //     <input value={} onChange={}/>
  //   </div>
  // },[])

  const [amountObj, setAmountObj] = useState({
    counter: 1,
    amount: 388000
  });

  const reduceDeal = () => {
    setAmountObj((state) => {
      return {
        counter: state.counter === 1 ? state.counter : state.counter - 1,
        amount: state.counter === 1 ? state.amount : state.amount - 388000
      }
    })
  }
  const increaseDeal = () => {
    setAmountObj((state) => {
      return {
        counter: state.counter + 1,
        amount: state.amount + 388000
      }
    })
  }

  const handleClose = () => {
    props.setVisible(false)
  }

  return (
    <Modal show={props.visible} onHide={handleClose} size="lg">
      <Modal.Header closeButton bsPrefix='main-login-content-modal-header' onClick={handleClose}/>
      <Modal.Body>
        <div className="item-modal-container">
          <div className="item-modal-body-left">
            {itemSlider()}
          </div>
          <div className="item-modal-body-right">
            <HeaderBoundingBox tag={tag} />
            <h3>Nước cân bằng chống oxy hóa lựu innisfree Jeju Pomegranate Revitalizing Toner 200ml</h3>
            <p className="item-modal-body-title">
              Nước cân bằng chống oxy hóa lựu innisfree Jeju Pomegranate Revitalizing Toner 200ml
            </p>
            <p className="item-modal-body-react">
              <div className="review-list-start">
                {renderStar}
              </div>
              <div className="review-general-footer">
                <img src={heart} alt="like" />
                <span>{props.detail.like}</span>
              </div>
            </p>
            <p className="item-modal-body-description">
              Nước cân bằng từ lựu đỏ innisfree Jeju Pomegranate Revitalizing Toner giúp cân bằng độ ẩm, dưỡng sáng và chống oxy hóa để làn da luôn tươi trẻ.
            </p>
            {renderFit(['DA', 'DS'])}
            <div className="item-modal-body-amount">
              <div className="item-modal-body-point">
                <h4>Điểm thưởng</h4>
                <ul>
                  <li>Welcome <span>2%</span></li>
                  <li>Premium <span>3%</span></li>
                  <li>VIP <span>4%</span></li>
                </ul>
              </div>
              <div className="item-modal-body-point">
                <h4>Số lượng</h4>
                <div className="item-modal-body-amount-info">
                  <div className="item-modal-body-counter">
                    <div className="item-modal-body-deal">
                      <a type="button" onClick={reduceDeal}>-</a>
                      <p>{amountObj.counter}</p>
                      <a type="button" onClick={increaseDeal}>+</a>
                    </div>
                  </div>
                  <div className="item-modal-body-amount">
                    <p className="item-modal-body-amount-display">
                      {FormatAmount(amountObj.amount)} VND
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="item-modal-body-btn-container">
              <div className="item-modal-body-btn">
                <div className="item-modal-body-btn-1">
                  <GeneralDisplayButtonBoxConverse des="Thêm vào giỏ hàng" />
                </div>
                <div className="item-modal-body-btn-2">
                  <GeneralDisplayButtonBox des="Xem chi tiết" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default ItemModal;