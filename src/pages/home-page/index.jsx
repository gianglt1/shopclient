import React, { useState } from 'react';
import LoginBar from './component/loginBar';
import SlideShow from './component/slide';
import MainContent from './component/content';
import { arrowDownShort } from 'assets/icon/icon';
import './style.css';
import ItemModal from './component/itemModal';


const HomePage = () => {
  const [visible, setVisible] = useState(false)

  const handleModal = () => {
    setVisible(true)
  }

  return (
    <div className="main">
      <div className="main-content">
        <LoginBar />
        <SlideShow />
        <div className="divide"><img src={arrowDownShort} alt="arrow-down" />Kéo xuống</div>
        <MainContent setVisible={handleModal}/>
        <ItemModal detail={{ star: 4, like: 100 }} setVisible={setVisible} visible={visible} />
      </div>

    </div>
  )
}

export default HomePage