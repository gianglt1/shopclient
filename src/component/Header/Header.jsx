import logo from 'assets/img/logo.svg';
import { geoAlt, search, person, cart } from 'assets/icon/icon.jsx';
import { Product, About } from 'component/Header/Modal/Modal';

const Header = () => {

    return (
        <div className="header-content">
            <nav>
                <ul>
                    <li className="under-line">
                        <a href="#">Sản phẩm</a>
                        <Product />
                    </li>
                    <li className="under-line"><a href="#">INNISTAR</a></li>
                    <li className="under-line"><a href="#">Sự kiện</a></li>
                    <li className="under-line"><a href="#">Thành viên</a></li>
                    <li className="under-line">
                        <a href="#">Về INNISTAR</a>
                        <About />
                    </li>
                </ul>
            </nav>
            <div id="header-logo">
                <a href="#"><img src={logo} alt="logo" /></a>
            </div>
            <ul>
                <li><a href="#" className="icon-decoration"><img className="img img1" src={geoAlt} alt="geoAlt" /></a></li>
                <li><a href="#" className="icon-decoration"><img className="img img2" src={search} alt="search" /></a></li>
                <li><a href="#" className="icon-decoration"><img className="img img3" src={person} alt="person" /></a></li>
                <li><a href="#" className="icon-decoration"><img className="img img4" src={cart} alt="cart" /></a></li>
            </ul>
        </div>
    )
}

export default Header;