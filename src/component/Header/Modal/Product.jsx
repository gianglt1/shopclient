




const Product = () => {

    return (
        <div className="header-modal">
            <div className="header-modal-content">
                <div className="header-modal-product-content-detail">
                    <div className="header-modal-product-list">
                        <div className="header-modal-product-list-title"><a href="#" className="under-line">Dưỡng da</a></div>
                        <ul>
                            <li><a href="#" className="under-line">Dưỡng da</a></li>
                            <li><a href="#" className="under-line">Sữa rữa mặt</a></li>
                            <li><a href="#" className="under-line">Nước hoa hồng</a></li>
                            <li><a href="#" className="under-line">Sữa dưỡng</a></li>
                            <li><a href="#" className="under-line">Tinh chất</a></li>
                            <li><a href="#" className="under-line">Dưỡng mắt</a></li>
                            <li><a href="#" className="under-line">Kem - Gel dưỡng</a></li>
                            <li><a href="#" className="under-line">Chống nằng</a></li>
                            <li><a href="#" className="under-line">Mặt nạ</a></li>
                            <li><a href="#" className="under-line">dưỡng môi</a></li>
                            <li><a href="#" className="under-line">Xịt khoáng</a></li>
                            <li><a href="#" className="under-line">Tẩy tế bào chết</a></li>
                            <li><a href="#" className="under-line">Bộ sản phẩm chăm sóc da</a></li>
                        </ul>
                    </div>
                    <div className="header-modal-product-list">
                        <div className="header-modal-product-list-element">
                            <div className="header-modal-product-list-title"><a href="#" className="under-line">Trang điểm</a></div>
                            <ul>
                                <li><a href="#" className="under-line">Dưỡng da</a></li>
                                <li><a href="#" className="under-line">Sữa rữa mặt</a></li>
                                <li><a href="#" className="under-line">Nước hoa hồng</a></li>
                                <li><a href="#" className="under-line">Sữa dưỡng</a></li>
                            </ul>
                        </div>
                        <div className="header-modal-product-list-element">
                            <div className="header-modal-product-list-title"><a href="#" className="under-line">Chăm sóc tóc & cơ thể</a></div>
                            <ul>
                                <li><a href="#" className="under-line">Dưỡng da</a></li>
                                <li><a href="#" className="under-line">Sữa rữa mặt</a></li>
                                <li><a href="#" className="under-line">Nước hoa hồng</a></li>
                                <li><a href="#" className="under-line">Sữa dưỡng</a></li>

                            </ul>
                        </div>
                        <div className="header-modal-product-list-element">
                            <div className="header-modal-product-list-title"><a href="#" className="under-line">Dành cho nam</a></div>
                            <ul>
                                <li><a href="#" className="under-line">Dưỡng da</a></li>
                                <li><a href="#" className="under-line">Sữa rữa mặt</a></li>
                                <li><a href="#" className="under-line">Nước hoa hồng</a></li>

                            </ul>
                        </div>
                    </div>
                    <div className="header-modal-product-list">
                        <div className="header-modal-product-list-element">
                            <div className="header-modal-product-list-title"><a href="#" className="under-line">Nguyên liệu</a></div>
                            <ul>
                                <li><a href="#" className="under-line">Dưỡng da</a></li>
                                <li><a href="#" className="under-line">Sữa rữa mặt</a></li>
                                <li><a href="#" className="under-line">Nước hoa hồng</a></li>
                                <li><a href="#" className="under-line">Sữa dưỡng</a></li>
                                <li><a href="#" className="under-line">Dưỡng mắt</a></li>
                                <li><a href="#" className="under-line">Kem - Gel dưỡng</a></li>
                                <li><a href="#" className="under-line">Chống nằng</a></li>
                                <li><a href="#" className="under-line">Mặt nạ</a></li>
                                <li><a href="#" className="under-line">dưỡng môi</a></li>
                                <li><a href="#" className="under-line">Xịt khoáng</a></li>
                                <li><a href="#" className="under-line">Tẩy tế bào chết</a></li>
                                <li><a href="#" className="under-line">Bộ sản phẩm chăm sóc da</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="header-modal-product-list">
                        <div className="header-modal-product-list-element">
                            <ul className="header-modal-product-list-specific">
                                <li><a href="#" className="under-line">Sản phẩm bán chạy</a></li>
                                <li><a href="#" className="under-line">Sản phẩm mới</a></li>
                                <li><a href="#" className="under-line">Độc quyền</a></li>
                                <li><a href="#" className="under-line">Sản phẩm ưu đãi</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product;
