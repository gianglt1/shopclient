import img1 from 'assets/img/about_ban1.png';
import img2 from 'assets/img/about_ban2.jpg';


const About = () => {


    return (
        <div className="header-modal">
            <div className="header-modal-content">
                <div className="header-modal-about-content-detail">
                    <div className="header-modal-about-list">
                        <h2>Về Innsisfree</h2>
                        <ul>
                            <li><a href="#" className="under-line">Câu chuyện thương hiệu</a></li>
                            <li><a href="#" className="under-line">Quá trình phát triển</a></li>
                            <li><a href="#" className="under-line">Câu chuyện thành phần</a></li>
                            <li><a href="#" className="under-line">Chiến dịch Green Forest</a></li>
                            <li><a href="#" className="under-line">Nghiên cứu & công nghệ</a></li>
                        </ul>
                    </div>
                    <div className="header-modal-about-image">
                        <a href="#" className="img1">
                            <img src={img1} alt="about_ban1" />
                            <p>Quá trình phát triển</p>
                        </a>
                        <a href="#" className="img2">
                            <img src={img2} alt="about_ban2" />
                            <p>Cậu chuyện thương hiệu</p>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default About;