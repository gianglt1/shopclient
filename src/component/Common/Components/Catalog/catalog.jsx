

import '../style.css';

const CatalogContent = (props) => {


    return (
        <div className="catalog">
            <div className="catalog-sub-title">
                <h2 className="title"><span>{props.title}</span></h2>
            </div>
            {props.children}
        </div>
    )
}

const CatalogDescriptionContent = (props) => {
    return (
        <div className="catalog-sub-description">
            {props.children}
        </div>
    )
}

export { CatalogContent, CatalogDescriptionContent };