



const Info = (props) => {

  return (
    <div className="info-box">
      <div className="info">
        <p>{props.info}</p>
      </div>
      <div className="amount">
        {props.sale && <p className="sale">VND {props.sale}.000</p>}
        <p className="origin"> VND {props.price}.000</p>
      </div>

    </div>
  )
}

const FitInfo = (props) => {

  return (
    <div className="fit-info">
      <img src={props.image} alt="image" />
      {props.info}
    </div>
  )
}

export default Info
export {
  FitInfo,
}