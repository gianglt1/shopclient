import React, { useMemo } from 'react';


const ListProduct = (props) => {

    const renderProduction = useMemo(() => {
        return Object.keys(props.detail || []).map((item, index) => {
            console.log(item)
            return <div className="story-production-item" key={index}>
                <div className="story-production-item-img">
                    <img src={props.detail[item]['image']} alt="greentea01" />
                </div>
                <div className="story-production-item-info">
                    {props.detail[item]['detail']}
                </div>
            </div>
        })

    }, [])


    return (
        <div className="story-production">
            <div className="story-production-wrap">
                {renderProduction}
            </div>
        </div>
    )
}

export default ListProduct;